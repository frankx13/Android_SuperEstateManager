package com.studio.neopanda.superestatemanager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.maps.android.ui.IconGenerator;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgentMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    //DATA
    private static final int MY_PERMISSIONS_REQUEST_GET_LOCATION = 1;
    //UI
    @BindView(R.id.agent_maps_properties_back_list_btn)
    Button goBackBtn;
    @BindView(R.id.navigation_view_maps)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout_maps)
    DrawerLayout drawerLayout;
    private GoogleMap mMap;
    private List<Item> listItem;
    private FusedLocationProviderClient fusedLocationClient;
    private int counter = 0;
    private float zoomLevel = 6.0f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_maps);

        ButterKnife.bind(this);

        catchIntentFromList();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setToolbar();
        setNavigationDrawer();
//        codeQR(this, this, new MyCallback() {
//            @Override
//            public void onCallback(Double result) {
//                Log.i("TestOfResult", "onCallback: " + result);
//            }
//        });
    }

    //Retrieving the item from the parcelable intent
    private void catchIntentFromList() {
        if (Parcels.unwrap(getIntent().getParcelableExtra("DataForMap")) != null) {
            listItem = Parcels.unwrap(getIntent().getParcelableExtra("DataForMap"));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GET_LOCATION);
                return;
            }
        }

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    // Got last known location.
                    if (location != null) {
                        // Add a marker in user location and move the camera
                        LatLng myLoc = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 6.0f));
                    } else {
                        Toast.makeText(AgentMapsActivity.this, "The system couldn't find your location!", Toast.LENGTH_SHORT).show();
                    }
                });

        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationClickListener(location -> Toast.makeText(AgentMapsActivity.this, "You are here!", Toast.LENGTH_LONG).show());

        if (listItem != null) {
            addMarkers(listItem);
        } else {
            Toast.makeText(this, "You need to launch the maps inside the Dashboard " +
                    "to get the properties list!", Toast.LENGTH_LONG).show();
        }
    }

    // ----MAP MANIPULATION----
    //Getting the latlng coordinates from the property address
    private LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng locationLatLng = null;

        //Geocoding algorithm
        try {
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                return null;
            }
            try {
                Address location = address.get(0);
                locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            } catch (IndexOutOfBoundsException e) {
                Log.i("OYOY", "getLocationFromAddressPROBLEM: " + address);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return locationLatLng;
    }

    //Position markers corresponding to each property in the database
    private void addMarkers(List<Item> itemsList) {
        //Implement icongenerator for markers
        IconGenerator iconFactory = new IconGenerator(this);
        if (itemsList != null) {
            //we loop inside the itemlist to affect a marker on each property
            for (Item i : listItem) {

                String address = i.getAddressOfProperty();
                String[] splittedAddressComplete = address.split(",", 5);
                String splittedAddresspartTown = splittedAddressComplete[1];
                String type = i.getTypeOfProperty();

                if (!address.equals("")) {
                    //we pass the address to the geocoding method
                    LatLng propertyLatLng = getLocationFromAddress(this, address);
                    if (mMap != null && propertyLatLng != null) {
                        Marker m = mMap.addMarker(new MarkerOptions()
                                .position(propertyLatLng)
//                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                                .icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(splittedAddresspartTown)))
                                .title(type));
                        m.setTag(i);

                        mMap.setOnMarkerClickListener(marker -> {
                            String name = marker.getTitle();

                            if (name.equalsIgnoreCase("Me!")) {
                                Toast.makeText(AgentMapsActivity.this, "That's your location!", Toast.LENGTH_SHORT).show();
                            } else {
                                Item item = (Item) marker.getTag();

                                if (counter < 2) {
                                    marker.showInfoWindow();
                                    counter++;
                                }

                                if (counter == 2) {
                                    goBackBtn.setVisibility(View.INVISIBLE);
                                    Fragment f = DetailPropertyFragment.newInstance(item);
                                    AgentMapsActivity.this.getSupportFragmentManager().beginTransaction().add(R.id.detailContainerFromAgentMaps, f).addToBackStack(null).commit();
                                    counter = 0;
                                }
                            }
                            return true;
                        });
                    }
                }
            }
        } else {
            Toast.makeText(this, "No address received from the Dashboard!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // ----ACTIONS----
    @OnClick(R.id.agent_maps_properties_zoom_btn)
    public void zoomPlusBtn() {
//        mMap.moveCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel += 1));
        Toast.makeText(this, "Zoom level : " + zoomLevel + " /20", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.agent_maps_properties_dezoom_btn)
    public void zoomMinusBtn() {
//        mMap.moveCamera(CameraUpdateFactory.zoomOut());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel -= 1));
        Toast.makeText(this, "Zoom level : " + zoomLevel + " /20", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.agent_maps_properties_back_list_btn)
    public void backToListBtn() {
        Toast.makeText(this, "Going back!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    //TOOLBAR
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_top_maps);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    //Toolbar functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return true;
    }

    //NAVIGATION DRAWER
    private void setNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            // set item as selected to persist highlight
            menuItem.setChecked(true);

            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.properties_dashboard_nd_maps:
                    Toast.makeText(this, "Going to the dashboard!", Toast.LENGTH_SHORT).show();
                    Intent intentSimulator = new Intent(this, DashboardActivity.class);
                    startActivity(intentSimulator);
                    finish();
                    break;
                case R.id.properties_mortgage_nd_maps:
                    Toast.makeText(this, "Going to the simulator!", Toast.LENGTH_SHORT).show();
                    Intent intentMap = new Intent(this, MortgageSimulatorActivity.class);
                    startActivity(intentMap);
                    finish();

                    break;
                case R.id.app_menu_nd_maps:
                    Toast.makeText(this, "Going to the menu!", Toast.LENGTH_SHORT).show();
                    Intent intentMenu = new Intent(this, MainActivity.class);
                    startActivity(intentMenu);
                    finish();
                    break;

                case R.id.test_connectivity_menu_nd_maps:
                    Toast.makeText(this, "Going to the Internet tester!", Toast.LENGTH_SHORT).show();
                    Intent intentInternetTest = new Intent(this, TestMyWifi.class);
                    startActivity(intentInternetTest);
                    finish();
                    break;
            }
            return true;
        });
    }


//    public interface MyCallback {
//        void onCallback(Double result);
//    }
//
//    public void codeQR(final Context context, final Activity activity, MyCallback myCallback) {
//
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//            }
//        }
//        fusedLocationClient.getLastLocation().addOnSuccessListener(activity, new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(Location location) {
//
//                latitude = location.getLatitude();
//                longitude = location.getLongitude();
//                result = latitude + longitude;
//
//                myCallback.onCallback(result);
//            }
//        });
//    }
}
