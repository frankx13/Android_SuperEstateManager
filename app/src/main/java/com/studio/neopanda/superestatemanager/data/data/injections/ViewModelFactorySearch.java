package com.studio.neopanda.superestatemanager.data.data.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.studio.neopanda.superestatemanager.data.data.repositories.ItemDataRepository;
import com.studio.neopanda.superestatemanager.data.data.repositories.UserDataRepository;
import com.studio.neopanda.superestatemanager.searchproperty.SearchViewModel;

public class ViewModelFactorySearch implements ViewModelProvider.Factory {

    private final ItemDataRepository itemDataSource;
    private final UserDataRepository userDataSource;

    public ViewModelFactorySearch(ItemDataRepository itemDataSource, UserDataRepository userDataSource) {
        this.itemDataSource = itemDataSource;
        this.userDataSource = userDataSource;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SearchViewModel.class)) {
            return (T) new SearchViewModel(itemDataSource, userDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
