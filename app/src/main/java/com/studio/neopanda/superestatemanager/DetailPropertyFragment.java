package com.studio.neopanda.superestatemanager;


import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailPropertyFragment extends Fragment {

    //UI
    @BindView(R.id.detail_activity_recyclerviewf)
    RecyclerView recyclerView;
    @BindView(R.id.detail_property_desc_TV_content_frag)
    TextView detailPropertyDescription;
    @BindView(R.id.detail_property_attribute_address_frag)
    TextView detailPropertyAttrAddress;
    @BindView(R.id.detail_property_attribute_arrival_on_market_date_frag)
    TextView detailPropertyAttrMarketArrival;
    @BindView(R.id.detail_property_attribute_agent_frag)
    TextView detailPropertyAttrAgent;
    @BindView(R.id.detail_property_attribute_nb_rooms_frag)
    TextView detailPropertyAttrRooms;
    @BindView(R.id.detail_property_attribute_surface_frag)
    TextView detailPropertyAttrSurface;
    @BindView(R.id.detail_property_map_IV_frag)
    ImageView detailPropertyMapIV;
    @BindView(R.id.detail_property_date_sold)
    TextView detailPropertyDateSold;

    //DATA
    private Item item;
    private List<Item> itemList;

    public DetailPropertyFragment() {
        // Required empty public constructor
    }

    public static DetailPropertyFragment newInstance(Item item) {
        DetailPropertyFragment myFragment = new DetailPropertyFragment();

        Bundle args = new Bundle();
        args.putParcelable("Item", Parcels.wrap(item));
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_property, container, false);
        if (getArguments() != null) {
            item = Parcels.unwrap(getArguments().getParcelable("Item"));
        }

        ButterKnife.bind(this, v);
        itemList = new ArrayList<>();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<String> listUrisRoom = item.getImagesOfRooms();

        if (listUrisRoom != null) {
            for (String str : listUrisRoom) {
                itemList.add(item);
            }
        }

        if (listUrisRoom != null) {
            for (String s : listUrisRoom) {
                configureRecyclerView(listUrisRoom);
            }
        }
        setTVTexts();

        if (Objects.requireNonNull(getActivity()).findViewById(R.id.detail_property_back_btn) != null) {
            onClickBackToPropertiesButton();
        }
    }

    private void setTVTexts() {
        String propertyAddress = item.getAddressOfProperty();
        String[] splittedAddressComplete = propertyAddress.split(",", 5);
        String splittedAddresspartTown = "";

        for (String s : splittedAddressComplete) {
            splittedAddresspartTown += s.trim();
            splittedAddresspartTown += "\n";
        }
        String agentAffectedToProperty = item.getNameAgentOfProperty();
        String arrivalPropertyOnMarket = item.getDateArrivalOnMarket();
        String surfaceProperty = String.valueOf(item.getSurfaceInSquareMeters());
        String fullDesc = item.getFullDescOfProperty();
        String baseRooms = String.valueOf(item.getNumberOfRooms());
        String dateSold = item.getDateSold();

        detailPropertyDescription.setText(fullDesc);
        detailPropertyAttrAddress.setText(detailPropertyAttrAddress.getText().toString() + "\n" + splittedAddresspartTown);
        detailPropertyAttrAgent.setText(detailPropertyAttrAgent.getText().toString() + "\n" + agentAffectedToProperty);
        detailPropertyAttrMarketArrival.setText(detailPropertyAttrMarketArrival.getText().toString() + "\n" + arrivalPropertyOnMarket);
        detailPropertyAttrSurface.setText(detailPropertyAttrSurface.getText().toString() + "\n" + surfaceProperty + " m²");
        detailPropertyAttrRooms.setText(detailPropertyAttrRooms.getText().toString() + "\n" + baseRooms);

        if (item.isSold()) {
            detailPropertyDateSold.setText(detailPropertyDateSold.getText().toString() + "\n" + dateSold);
            detailPropertyDateSold.setVisibility(View.VISIBLE);
        }

        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocationName(propertyAddress, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        double latitude = 0;
        double longitude = 0;
        if (addresses != null) latitude = addresses.get(0).getLatitude();

        if (addresses != null) longitude = addresses.get(0).getLongitude();

        if (addresses != null) {
            positionOnMap(latitude, longitude);
        } else {
            Toast.makeText(getActivity(), "We couldn't determine the location of this address. " +
                    "Try to modify it respecting the information indicator", Toast.LENGTH_SHORT).show();
        }
    }

    //Building the URL of bitmap from map
    private void positionOnMap(double latitude, double longitude) {
        String urlTest = "http://maps.googleapis.com/maps/api/staticmap?";
        urlTest += "center=" + latitude + "," + longitude;
        urlTest += "&zoom=10";
        urlTest += "&markers=color:red%7Clabel:Property%7C" + latitude + "," + longitude;
        urlTest += "&size=250x250";
        urlTest += "&maptype=roadmap";
        urlTest += "&key=AIzaSyATJKIALm460NEm9mg2zK3O9YzBq2N0YWg";

        Glide.with(this).load(urlTest).into(detailPropertyMapIV);
    }

    private void onClickBackToPropertiesButton() {
        Button backBtn = Objects.requireNonNull(getActivity()).findViewById(R.id.detail_property_back_btn);
        backBtn.setOnClickListener(v -> {
            Toast.makeText(getActivity(), "Going back to the list!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), DashboardActivity.class);
            startActivity(intent);
        });
    }

    private void configureRecyclerView(List<String> listsOfUris) {
        DetailAdapter recyclerAdapter = new DetailAdapter(getContext(), listsOfUris, itemList);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerAdapter.notifyDataSetChanged();
    }
}
