package com.studio.neopanda.superestatemanager;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;
import com.studio.neopanda.superestatemanager.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class TestMyWifi extends BaseActivity {

    @BindView(R.id.test_network_signal_positive_ET)
    TextView networkSignalPositive;
    @BindView(R.id.test_network_signal_negative_ET)
    TextView networkSignalNegative;
    @BindView(R.id.test_network_signal_text_ET)
    TextView networkSignalText;
    @BindView(R.id.navigation_view_network)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout_network)
    DrawerLayout drawerLayout;

    String signalLevel;
    private boolean isInternetAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        testWifi();
        Toast.makeText(this, "Test done!", Toast.LENGTH_SHORT).show();

        setToolbar();
        setNavigationDrawer();
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_test_my_network;
    }

    public int getWifiLevel() {
        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        return WifiManager.calculateSignalLevel(linkSpeed, 5);
    }

    private void testWifi() {
        getWifiLevel();
        if (getWifiLevel() == 1) {
            signalLevel = "very bad";
            networkSignalText.setText(getString(R.string.your_internet_is, signalLevel));
        } else if (getWifiLevel() == 2) {
            signalLevel = "bad";
            networkSignalText.setText(getString(R.string.your_internet_is, signalLevel));
            networkSignalPositive.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 10f));
            networkSignalNegative.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f));
        } else if (getWifiLevel() == 3) {
            signalLevel = "correct";
            networkSignalText.setText(getString(R.string.your_internet_is, signalLevel));
            networkSignalPositive.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f));
            networkSignalNegative.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f));
        } else if (getWifiLevel() == 4) {
            signalLevel = "good";
            networkSignalText.setText(getString(R.string.your_internet_is, signalLevel));
            networkSignalPositive.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
            networkSignalNegative.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f));
        } else if (getWifiLevel() == 5) {
            signalLevel = "very good";
            networkSignalText.setText(getString(R.string.your_internet_is, signalLevel));
            networkSignalPositive.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.1f));
            networkSignalNegative.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f));
        }
    }

    @OnClick(R.id.test_network_again_btn)
    public void relaunchTest() {
        testWifi();
        Toast.makeText(this, "Redoing the test!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.test_network_back_btn)
    public void backBtn() {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    //TOOLBAR
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_top_network);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    //Toolbar functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return true;
    }

    //NAVIGATION DRAWER
    private void setNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            // set item as selected to persist highlight
            menuItem.setChecked(true);

            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.properties_dashboard_nd_network:
                    Toast.makeText(this, "Going to the Dashboard!", Toast.LENGTH_SHORT).show();
                    Intent intentSimulator = new Intent(this, DashboardActivity.class);
                    startActivity(intentSimulator);
                    finish();
                    break;
                case R.id.properties_mortgage_nd_network:
                    Toast.makeText(this, "Going to the Simulator!", Toast.LENGTH_SHORT).show();
                    Intent intentMap = new Intent(this, MortgageSimulatorActivity.class);
                    startActivity(intentMap);
                    finish();

                    break;
                case R.id.app_menu_nd_network:
                    Toast.makeText(this, "Going to the Menu!", Toast.LENGTH_SHORT).show();
                    Intent intentMenu = new Intent(this, MainActivity.class);
                    startActivity(intentMenu);
                    finish();
                    break;

                case R.id.maps_menu_nd_network:
                    if (Utils.isConnectedToNetwork(this, isInternetAvailable)) {

                        Toast.makeText(this, "Going to the Map!", Toast.LENGTH_SHORT).show();
                        Intent intentInternetTest = new Intent(this, AgentMapsActivity.class);
                        startActivity(intentInternetTest);
                        finish();
                    } else {
                        Toast.makeText(this, "Oops ! It seems you do not receive Internet. Try again when you get some!", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
            return true;
        });
    }
}
