package com.studio.neopanda.superestatemanager.listproperty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private final Listener callback;
    // FOR DATA
    private List<Item> items;
    private boolean isTablet;

    // CONSTRUCTOR
    public ItemAdapter(Listener callback, boolean isTablet) {
        this.items = new ArrayList<>();
        this.callback = callback;
        this.isTablet = isTablet;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_dashboard_item, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int position) {
        viewHolder.updateWithItem(this.items.get(position), this.callback);
        viewHolder.handleClickOnItem(this.items.get(position), isTablet);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public void updateData(List<Item> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    // CALLBACK
    public interface Listener {
        void onClickDeleteButton(int position);
    }
}