package com.studio.neopanda.superestatemanager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.data.data.injection.Injection;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactory;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;
import com.studio.neopanda.superestatemanager.listproperty.ItemViewModel;

import org.parceler.Parcels;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class EditPropertyActivity extends BaseActivity {

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static int GALLERY_REQUEST_CODE = 1;
    //UI
    @BindView(R.id.edit_property_type)
    EditText propertyTypeET;
    @BindView(R.id.edit_property_address)
    EditText propertyLocalisationET;
    @BindView(R.id.edit_property_price)
    EditText propertyPriceET;
    @BindView(R.id.edit_property_surface)
    EditText propertySurfaceET;
    @BindView(R.id.edit_property_description)
    EditText propertyDescET;
    @BindView(R.id.edit_property_name_agent)
    EditText propertyAffectedAgentET;
    @BindView(R.id.edit_property_number_rooms)
    EditText propertyNumberOfRoomsET;
    @BindView(R.id.edit_property_date_arrival_market)
    EditText propertyAvailableMarketET;
    @BindView(R.id.edit_property_switch_sold)
    Switch isSold;
    @BindView(R.id.edit_property_sold_date)
    EditText propertyDateSoldET;
    @BindView(R.id.edit_property_create_btn)
    Button propertyCreateBtn;
    @BindView(R.id.edit_property_sold_tv)
    TextView propertySoldTV;
    @BindView(R.id.edit_property_sold_graphics)
    LinearLayout propertySoldUIContainer;

    //DATA
    private Item item;
    private ItemViewModel itemViewModel;
    private int URI_REQUEST_CODE;
    private List<String> urisOfImageGalleryForRooms;
    private List<String> imagesForRoomsPresent;
    private List<String> imagesForRoomsAdded;
    private List<String> urisOfRoomsFreshlyAdded;
    private List<String> emptyStringList;
    private String uriOfImageGallery = "";
    private Uri uriForCoverString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        urisOfImageGalleryForRooms = new ArrayList<>();
        urisOfRoomsFreshlyAdded = new ArrayList<>();
        imagesForRoomsPresent = new ArrayList<>();
        imagesForRoomsAdded = new ArrayList<>();

        emptyStringList = new ArrayList<>();
        item = Parcels.unwrap(getIntent().getParcelableExtra("Item"));

        this.configureToolbar();
        this.configureViewModel();
        this.displayText();
    }

    //Configuring ViewModel
    private void configureViewModel() {
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(this);
        this.itemViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ItemViewModel.class);
        int USER_ID = 1;
        this.itemViewModel.init(USER_ID);
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_edit_property;
    }

    private void displayText() {
        String typeProperty = item.getTypeOfProperty();
        String localisationProperty = item.getAddressOfProperty();
        String priceProperty = item.getPriceInDollar();
        String surfaceProperty = String.valueOf(item.getSurfaceInSquareMeters());
        String descriptionProperty = item.getFullDescOfProperty();
        String affectedAgentProperty = item.getNameAgentOfProperty();
        String numberOfRoomsProperty = String.valueOf(item.getNumberOfRooms());
        String datePutProperty = item.getDateArrivalOnMarket();
        String dateSoldProperty = item.getDateSold();

        propertyTypeET.setText(typeProperty);
        propertyLocalisationET.setText(localisationProperty);
        propertyPriceET.setText(priceProperty);
        propertySurfaceET.setText(surfaceProperty);
        propertyDescET.setText(descriptionProperty);
        propertyAffectedAgentET.setText(affectedAgentProperty);
        propertyNumberOfRoomsET.setText(numberOfRoomsProperty);
        propertyAvailableMarketET.setText(datePutProperty);
        propertyDateSoldET.setText(dateSoldProperty);

        if (!item.isSold()) {
            isSold.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isSold.isChecked()) {
                    propertyDateSoldET.setVisibility(View.VISIBLE);
                } else {
                    propertyDateSoldET.setVisibility(View.GONE);
                }
            });
        } else {
            isSold.setVisibility(View.GONE);
            propertyCreateBtn.setVisibility(View.GONE);
            propertySoldTV.setTextColor(ContextCompat.getColor(this.getApplicationContext(),
                    R.color.colorPrimary));
            propertySoldTV.setText(getString(R.string.property_sold_ruban));
            propertySoldTV.setTextSize(45);
            propertySoldTV.setGravity(Gravity.CENTER);
            propertySoldUIContainer.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
            propertySoldUIContainer.requestLayout();
        }
    }

    private void updateItem(Item item) {
        this.itemViewModel.updateItem(item);
        Toast.makeText(this, "Propriété modifiée avec succès!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.edit_property_create_btn)
    public void editPropertyValidationButton() {
        int securityValidationCounter = 0;
        String typeProperty = propertyTypeET.getEditableText().toString();
        String localisationProperty = propertyLocalisationET.getEditableText().toString();
        String priceProperty = propertyPriceET.getEditableText().toString();
        String imageProperty = uriOfImageGallery;
        int surfaceProperty = Integer.parseInt(propertySurfaceET.getEditableText().toString());
        String descriptionProperty = propertyDescET.getEditableText().toString();
        String affectedAgentProperty = propertyAffectedAgentET.getEditableText().toString();
        int numberOfRoomsProperty = Integer.parseInt(propertyNumberOfRoomsET.getEditableText().toString());
        String datePutProperty = propertyAvailableMarketET.getEditableText().toString();
        String dateSoldProperty = propertyDateSoldET.getEditableText().toString();

        urisOfImageGalleryForRooms = item.getImagesOfRooms();
        imagesForRoomsPresent = urisOfImageGalleryForRooms;
        imagesForRoomsAdded = urisOfRoomsFreshlyAdded;

        if (!typeProperty.equals("")){
            item.setTypeOfProperty(typeProperty);
            securityValidationCounter +=1;
        }
        else
            Toast.makeText(this, "The property type is empty, " +
                    "edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!localisationProperty.equals("")){
            item.setAddressOfProperty(localisationProperty);
            securityValidationCounter +=1;
        }
        else
            Toast.makeText(this, "The property address is empty, " +
                    "edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!String.valueOf(surfaceProperty).equals("")){
            item.setSurfaceInSquareMeters(surfaceProperty);
            securityValidationCounter +=1;
        }
        else
            Toast.makeText(this, "The property surface is empty, " +
                    "edit with a correct value please!", Toast.LENGTH_SHORT).show();


        if (!descriptionProperty.equals("")){
            item.setFullDescOfProperty(descriptionProperty);
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property description is empty, " +
                "edit with a correct value please!", Toast.LENGTH_SHORT).show();


        if (!affectedAgentProperty.equals("")){
            item.setNameAgentOfProperty(affectedAgentProperty);
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property affected agent is empty, " +
                "edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!String.valueOf(numberOfRoomsProperty).equals("")){
            item.setNumberOfRooms(numberOfRoomsProperty);
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property number of rooms is empty, edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!datePutProperty.equals("")){
            item.setDateArrivalOnMarket(datePutProperty);
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property arrival date is empty, edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (imagesForRoomsPresent.size() == 0) {
            if (imagesForRoomsAdded.size() == 0) {
                item.setImagesOfRooms(emptyStringList);
            } else {
                imagesForRoomsPresent.addAll(imagesForRoomsAdded);
                item.setImagesOfRooms(imagesForRoomsPresent);
            }
        } else {
            if (imagesForRoomsAdded.size() == 0) {
                item.setImagesOfRooms(imagesForRoomsPresent);
            } else {
                for (int i = 0; i < imagesForRoomsPresent.size(); i++) {
                    for (int j = 0; j < imagesForRoomsAdded.size(); j++) {
                        if (imagesForRoomsPresent.get(i).equals(imagesForRoomsAdded.get(j))) {
                            imagesForRoomsPresent.set(imagesForRoomsPresent.indexOf(imagesForRoomsPresent.get(i)), imagesForRoomsAdded.get(j));
                        } else {
                            imagesForRoomsPresent.add(imagesForRoomsAdded.get(j));
                        }
                    }
                }
                item.setImagesOfRooms(imagesForRoomsPresent);
                securityValidationCounter +=1;
            }

        }

        if (!item.getImageOfProperty().equals("")){
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property main image is empty, edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!priceProperty.equals("")){
            item.setPriceInDollar(priceProperty);
            securityValidationCounter +=1;
        }
        else Toast.makeText(this, "The property price is empty, edit with a correct value please!", Toast.LENGTH_SHORT).show();

        if (!dateSoldProperty.equals("")){
            item.setDateSold(dateSoldProperty);
        }
        else {
            if (propertyDateSoldET.getVisibility() == View.GONE)
            Toast.makeText(this, "The property date of sell is empty, edit with a correct value please!", Toast.LENGTH_SHORT).show();
        }

        if (isSold.isChecked()) item.setSold(true);

        Log.i("COUNTER", "editPropertyValidationButton: " + securityValidationCounter);

        if (securityValidationCounter >= 9){
            this.updateItem(item);
            Toast.makeText(this, "The property has been modified!", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.edit_property_cover_gallery_btn)
    public void editImageCoverWithGallery() {
        pickFromGallery(1);
    }

    @OnClick(R.id.edit_property_rooms_gallery_btn)
    public void editImageRoomWithPicture() {
        pickFromGallery(2);
    }

    @OnClick(R.id.edit_property_cover_picture_btn)
    public void editImageCoverWithCamera() {
        dispatchTakePictureIntent(3);
    }

    @OnClick(R.id.edit_property_rooms_picture_btn)
    public void editImageRoomWithCamera() {
        dispatchTakePictureIntent(4);
    }

    @OnClick(R.id.edit_property_back_btn)
    public void onClickBackBtn() {
        Toast.makeText(this, "Going back to the list", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    // ----GALLERY----
    @SuppressLint("ObsoleteSdkInt")
    private void pickFromGallery(int galleryCode) {
        URI_REQUEST_CODE = galleryCode;
        Intent intento;
        if (Build.VERSION.SDK_INT < 19) {
            intento = new Intent();
            intento.setAction(Intent.ACTION_GET_CONTENT);
            intento.setType("image/*");
            startActivityForResult(intento, GALLERY_REQUEST_CODE);
        } else {
            intento = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intento.addCategory(Intent.CATEGORY_OPENABLE);
            intento.setType("image/*");
            startActivityForResult(intento, GALLERY_REQUEST_CODE);
        }
    }

    // ----CAMERA----
    private void dispatchTakePictureIntent(int captureCode) {
        URI_REQUEST_CODE = captureCode;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.studio.neopanda.superestatemanager",
                        photoFile);
                uriForCoverString = photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // Save a file: path for use with ACTION_VIEW intents
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == GALLERY_REQUEST_CODE) {
                if (URI_REQUEST_CODE == 1) {
                    Uri selectedImage = data.getData();
                    if (selectedImage != null) {
                        uriOfImageGallery = selectedImage.toString();
                        Toast.makeText(this, selectedImage.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 2) {
                    Uri selectedImage = data.getData();
                    if (selectedImage != null) {
                        urisOfImageGalleryForRooms.add(selectedImage.toString());
                        urisOfRoomsFreshlyAdded.add(selectedImage.toString());
                        Toast.makeText(this, selectedImage.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 3) {
                    if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                        uriOfImageGallery = uriForCoverString.toString();
                        Toast.makeText(this, uriForCoverString.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 4) {
                    if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                        urisOfImageGalleryForRooms.add(uriForCoverString.toString());
                        urisOfRoomsFreshlyAdded.add(uriForCoverString.toString());
                        Toast.makeText(this, uriForCoverString.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "GET_ACCOUNTS Denied",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions,
                    grantResults);
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                (dialog, which) -> ActivityCompat.requestPermissions((Activity) context,
                        new String[]{permission},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE));
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}