package com.studio.neopanda.superestatemanager.listproperty;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.studio.neopanda.superestatemanager.AddPropertyActivity;
import com.studio.neopanda.superestatemanager.AgentMapsActivity;
import com.studio.neopanda.superestatemanager.MainActivity;
import com.studio.neopanda.superestatemanager.MortgageSimulatorActivity;
import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.TestMyWifi;
import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.data.data.injection.Injection;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactory;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.searchproperty.SearchPropertyActivity;
import com.studio.neopanda.superestatemanager.utils.Utils;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;

public class DashboardActivity extends BaseActivity implements ItemAdapter.Listener {

    private static int USER_ID = 1;
    //UI
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.properties_recyclerview)
    RecyclerView recyclerView;

    //DATA
    private ItemAdapter adapter;
    private ItemViewModel itemViewModel;
    private boolean isInternetAvaible;

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_dashboard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.DashBoardTheme);
        super.onCreate(savedInstanceState);

        this.configureViewModel();
        this.getItems(USER_ID);
        boolean isTwoPane = findViewById(R.id.detailContainer) != null;
        this.configureRecyclerView(isTwoPane);
        this.setToolbar();
    }

    //TOOLBAR
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    //Toolbar setup
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_options, menu);
        return true;
    }

    //Toolbar functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.search_option:
                Toast.makeText(this, "Going to the search engine!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, SearchPropertyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            case R.id.create_option:
                Toast.makeText(this, "Going to the property creation page!", Toast.LENGTH_LONG).show();
                startActivity(new Intent(this, AddPropertyActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //NAVIGATION DRAWER
    private void setNavigationDrawer(List<Item> items) {
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            // set item as selected to persist highlight
            menuItem.setChecked(true);

            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.mortgage_sim_nd:
                    Toast.makeText(this, "Going to the simulator!", Toast.LENGTH_SHORT).show();
                    Intent intentSimulator = new Intent(this, MortgageSimulatorActivity.class);
                    startActivity(intentSimulator);
                    finish();
                    break;
                case R.id.properties_map_nd:
                    if (Utils.isConnectedToNetwork(this, isInternetAvaible)) {
                        Toast.makeText(this, "Going to the interactive map!", Toast.LENGTH_SHORT).show();
                        Intent intentMap = new Intent(this, AgentMapsActivity.class);
                        intentMap.putExtra("DataForMap", Parcels.wrap(items));
                        startActivity(intentMap);
                        finish();
                    } else {
                        Toast.makeText(this, "Oops ! It seems you do not receive Internet. Try again when you get some!", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case R.id.properties_menu_nd:
                    Toast.makeText(this, "Going to the main menu!", Toast.LENGTH_SHORT).show();
                    Intent intentMenu = new Intent(this, MainActivity.class);
                    startActivity(intentMenu);
                    finish();
                    break;

                case R.id.test_connectivity_menu_nd:
                    Toast.makeText(this, "Going to the Internet tester!", Toast.LENGTH_SHORT).show();
                    Intent intentInternetTest = new Intent(this, TestMyWifi.class);
                    startActivity(intentInternetTest);
                    finish();
                    break;
            }
            return true;
        });
    }

    //Configuring ViewModel
    private void configureViewModel() {
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(this);
        this.itemViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ItemViewModel.class);
        this.itemViewModel.init(USER_ID);
    }

    //Get all items for a user
    private void getItems(int userId) {
        this.itemViewModel.getItems(userId).observe(this, this::updateItemsList);
    }

    //Update the list of items
    private void updateItemsList(List<Item> items) {
        this.adapter.updateData(items);
        setNavigationDrawer(items);
    }

    //Configure RecyclerView
    private void configureRecyclerView(boolean isTwoPane) {
        this.adapter = new ItemAdapter(this, isTwoPane);
        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClickDeleteButton(int position) {

    }
}
