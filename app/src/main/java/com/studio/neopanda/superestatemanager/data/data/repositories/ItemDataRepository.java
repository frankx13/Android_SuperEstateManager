package com.studio.neopanda.superestatemanager.data.data.repositories;

import androidx.lifecycle.LiveData;

import com.studio.neopanda.superestatemanager.data.data.dao.ItemDao;
import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.util.List;

public class ItemDataRepository {

    private final ItemDao itemDao;

    public ItemDataRepository(ItemDao itemDao) {
        this.itemDao = itemDao;
    }

    // --- GET ---

    public LiveData<List<Item>> getItems(long userId) {
        return this.itemDao.getItems(userId);
    }

    // --- SEARCH ---
    public LiveData<List<Item>> searchItems(long userId,
                                            int priceInDollarMax,
                                            int priceInDollarMin,
                                            int surfaceInSquareMetersMax,
                                            int surfaceInSquareMetersMin,
                                            int numberOfRoomsMax,
                                            int numberOfRoomsMin) {
        return this.itemDao.searchItems(userId,
                priceInDollarMax,
                priceInDollarMin,
                surfaceInSquareMetersMax,
                surfaceInSquareMetersMin,
                numberOfRoomsMax,
                numberOfRoomsMin);
    }


    // --- CREATE ---

    public void createItem(Item item) {
        itemDao.insertItem(item);
    }

    // --- UPDATE ---
    public void updateItem(Item item) {
        itemDao.updateItem(item);
    }

}