package com.studio.neopanda.superestatemanager.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;


public class Utils {

    public static int convertDollarToEuro(int dollars) {
        return (int) Math.round(dollars * 0.90); //Valeur aout 2019
    }

    public static int convertEuroToDollars(int euros) {
        return (int) Math.round(euros * 1.11); //Valeur aout 2019
    }

    public static String getTodayDate() {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(new Date());
    }

    //ENHANCED METHOD TO CHECK INTERNET CONNEXION
    public static boolean isConnectedToNetwork(Context context, boolean isInternetAvailable) {
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {
            if (Objects.requireNonNull(connec).isDefaultNetworkActive()){
                isInternetAvailable = true;
                return isInternetAvailable;
            }
        } else if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED))) {

            Toast.makeText(context.getApplicationContext(), "You must be connected to the internet", Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (connec != null) {
                Log.i("Test", "isConnectedToNetwork: " + connec.isDefaultNetworkActive());
            }
            return Objects.requireNonNull(connec).isDefaultNetworkActive();
        } else {
            //if else to check for versions less than API 21
            return false;
        }
    }
}
