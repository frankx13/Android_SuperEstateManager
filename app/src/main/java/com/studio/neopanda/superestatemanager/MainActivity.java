package com.studio.neopanda.superestatemanager;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.data.data.injection.Injection;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactory;
import com.studio.neopanda.superestatemanager.data.data.models.User;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;
import com.studio.neopanda.superestatemanager.listproperty.ItemViewModel;
import com.studio.neopanda.superestatemanager.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    private static int USER_ID = 1;
    //UI
    @BindView(R.id.datetoday)
    TextView dateOfToday;
    @BindView(R.id.profile_image_user_main)
    ImageView profileImageMainUser;
    @BindView(R.id.profile_text_user_main)
    TextView profileTextMainUser;
    @BindView(R.id.create_user_username)
    EditText createUsername;
    @BindView(R.id.create_user_id)
    EditText createId;
    @BindView(R.id.login_btn)
    Button loginUserBtn;
    @BindView(R.id.navigation_view_main)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout_main)
    DrawerLayout drawerLayout;

    //DATA
    private ItemViewModel itemViewModel;
    private boolean isInternetAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configureViewModel();
        getCurrentUser(USER_ID);
        displayDateOfToday();
        setToolbar();
        setNavigationDrawer();
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_main;
    }

    //Display date
    public void displayDateOfToday() {
        String currentDate = Utils.getTodayDate();
        dateOfToday.setText(currentDate);
    }

    //Actions
    @OnClick(R.id.properties_list_btn)
    public void onClickToDashboardButton() {
        Toast.makeText(this, "Going to the list!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.properties_mortgage_btn)
    public void onClickMortgageSimulatorBtn() {
        Toast.makeText(this, "Going to the simulator!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MortgageSimulatorActivity.class);
        startActivity(intent);
        finish();
    }

    //Get Current User
    private void getCurrentUser(int userId) {
        this.itemViewModel.getUser(userId).observe(this, this::updateHeader);
    }

    private void createUser() {
        String username = createUsername.getEditableText().toString();
        int id = Integer.parseInt(createId.getEditableText().toString());
        String userPicture = "https://static.lexpress.fr/medias_1849/w_960,h_540,c_fill,g_north/v1376048689/hulk-1_946820.jpg?auto=compress,format&q=80&h=100&dpr=2";

        User user = new User(id, username, userPicture);

        this.itemViewModel.createUser(user);
    }

    //Update header (username & picture)
    private void updateHeader(User user) {
        if (user == null) {
            createUsername.setVisibility(View.VISIBLE);
            createId.setVisibility(View.VISIBLE);
            loginUserBtn.setVisibility(View.VISIBLE);
        } else {
            createUsername.setVisibility(View.INVISIBLE);
            createId.setVisibility(View.INVISIBLE);
            loginUserBtn.setVisibility(View.INVISIBLE);

            this.profileTextMainUser.setText(user.getUsername());
            if (user.getUrlPicture().isEmpty()) {
                Drawable myDrawable = getResources().getDrawable(R.drawable.ghost_image);
                this.profileImageMainUser.setImageDrawable(myDrawable);
            } else {
                Glide.with(this).load(user.getUrlPicture())
                        .apply(RequestOptions.circleCropTransform())
                        .into(this.profileImageMainUser);
            }
        }
    }

    //Configuring ViewModel
    private void configureViewModel() {
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(this);
        this.itemViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ItemViewModel.class);
        this.itemViewModel.init(USER_ID);
    }

    @OnClick(R.id.login_btn)
    public void loginBtnClick() {
        createUser();
    }

    //NAVIGATION DRAWER
    private void setNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            // set item as selected to persist highlight
            menuItem.setChecked(true);

            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.mortgage_sim_nd_main:
                    Toast.makeText(this, "Going to the simulator!", Toast.LENGTH_SHORT).show();
                    Intent intentSimulator = new Intent(this, MortgageSimulatorActivity.class);
                    startActivity(intentSimulator);
                    finish();
                    break;
                case R.id.properties_map_nd_main:
                    if (Utils.isConnectedToNetwork(this, isInternetAvailable)) {
                        Toast.makeText(this, "Going to the interactive map!", Toast.LENGTH_SHORT).show();
                        Intent intentMap = new Intent(this, AgentMapsActivity.class);
                        startActivity(intentMap);
                        finish();
                    } else {
                        Toast.makeText(this, "Oops ! It seems you do not receive Internet. Try again when you get some!", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case R.id.properties_dashboard_nd_main:
                    Toast.makeText(this, "Going to the Dashboard!", Toast.LENGTH_SHORT).show();
                    Intent intentMenu = new Intent(this, DashboardActivity.class);
                    startActivity(intentMenu);
                    finish();
                    break;

                case R.id.test_connectivity_menu_nd_main:
                    Toast.makeText(this, "Going to the Internet tester!", Toast.LENGTH_SHORT).show();
                    Intent intentInternetTest = new Intent(this, TestMyWifi.class);
                    startActivity(intentInternetTest);
                    finish();
                    break;
            }
            return true;
        });
    }

    //Toolbar functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return true;
    }

    //TOOLBAR
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }
}
