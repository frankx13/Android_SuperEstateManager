package com.studio.neopanda.superestatemanager.data.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.util.List;

@Dao
public interface ItemDao {

    //To get all properties
    @Query("SELECT * FROM Item WHERE userId = :userId")
    LiveData<List<Item>> getItems(long userId);

    //To search specific properties
    @Query("SELECT * FROM Item WHERE userId =:userId " +
            "AND ((numberOfRooms >:numberOfRoomsMin AND numberOfRooms <:numberOfRoomsMax) " +
            "OR (surfaceInSquareMeters <:surfaceInSquareMetersMax AND surfaceInSquareMeters >:surfaceInSquareMetersMin) " +
            "OR (priceInDollar <:priceInDollarMax AND priceInDollar >:priceInDollarMin)) " +
            "OR ((numberOfRooms >:numberOfRoomsMin AND numberOfRooms <:numberOfRoomsMax AND priceInDollar >:priceInDollarMin AND priceInDollar <:priceInDollarMax)" +
            "OR (numberOfRooms >:numberOfRoomsMin AND numberOfRooms <:numberOfRoomsMax AND surfaceInSquareMeters <:surfaceInSquareMetersMax AND surfaceInSquareMeters >:surfaceInSquareMetersMin)" +
            "OR (priceInDollar <:priceInDollarMax AND priceInDollar >:priceInDollarMin AND surfaceInSquareMeters <:surfaceInSquareMetersMax AND surfaceInSquareMeters >:surfaceInSquareMetersMin))" +
            "OR ((priceInDollar <:priceInDollarMax AND priceInDollar >:priceInDollarMin AND surfaceInSquareMeters <:surfaceInSquareMetersMax AND surfaceInSquareMeters >:surfaceInSquareMetersMin AND numberOfRooms >:numberOfRoomsMin AND numberOfRooms <:numberOfRoomsMax))")
    LiveData<List<Item>> searchItems(long userId,
                                     int priceInDollarMax,
                                     int priceInDollarMin,
                                     int surfaceInSquareMetersMax,
                                     int surfaceInSquareMetersMin,
                                     int numberOfRoomsMax,
                                     int numberOfRoomsMin);

    //To add properties
    @Insert
    long insertItem(Item item);

    //To update properties
    @Update
    int updateItem(Item item);
}