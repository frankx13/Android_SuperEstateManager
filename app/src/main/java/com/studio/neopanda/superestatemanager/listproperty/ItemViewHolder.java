package com.studio.neopanda.superestatemanager.listproperty;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.DetailPropertyFragment;
import com.studio.neopanda.superestatemanager.EditPropertyActivity;
import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.utils.Utils;

import org.parceler.Parcels;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.rl_property)
    RelativeLayout containerProperty;
    @BindView(R.id.img_property)
    ImageView imageProperty;
    @BindView(R.id.type_property)
    TextView typeProperty;
    @BindView(R.id.localisation_property)
    TextView localisationProperty;
    @BindView(R.id.price_property)
    TextView priceProperty;
    @BindView(R.id.img_property_when_sold)
    TextView labelSoldTV;
    @BindView(R.id.date_sold_property)
    TextView dateSoldPropertyTV;


    // FOR DATA
    private WeakReference<ItemAdapter.Listener> callbackWeakRef;
    private Context context = itemView.getContext();
    private Boolean isDollarCurrencyDisplayed = true;
    private Item item;

    public ItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(v -> Toast.makeText(context, "Test", Toast.LENGTH_SHORT).show());
    }

    public void updateWithItem(Item item, ItemAdapter.Listener callback) {
        this.item = item;
        this.callbackWeakRef = new WeakReference<>(callback);

        if (item.isSold()) {
            dateSoldPropertyTV.setText(itemView.getResources().getString(R.string.sold_the_string_date, item.getDateSold()));
            dateSoldPropertyTV.setVisibility(View.VISIBLE);
            labelSoldTV.setVisibility(View.VISIBLE);
        }

        if (!item.getImageOfProperty().equals("")) {
            this.imageProperty.setImageURI(Uri.parse(item.getImageOfProperty()));
        } else {
            this.imageProperty.setBackgroundResource(R.drawable.ic_edit_black_24dp);
        }

        this.typeProperty.setText(item.getTypeOfProperty());
        String[] splittedAddressComplete = item.getAddressOfProperty().split(",");
        String splittedAddresspart = splittedAddressComplete[0];
        this.localisationProperty.setText(splittedAddresspart);
        this.priceProperty.setText(item.getPriceInDollar());
    }

    public void handleClickOnItem(Item item, boolean isTablet) {
        this.item = item;
        itemView.setOnClickListener(v -> {
            if (!isTablet) {
                Toast.makeText(context, "Going to the property detail!", Toast.LENGTH_SHORT).show();
            }
            if (isTablet) {
                AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                Fragment f = DetailPropertyFragment.newInstance(item);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.detailContainer, f).addToBackStack(null).commit();

            } else {
                AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                Fragment f = DetailPropertyFragment.newInstance(item);
                activity.getSupportFragmentManager().beginTransaction().add(R.id.detailContainer, f).addToBackStack(null).commit();

            }
        });
    }

    @Override
    public void onClick(View view) {
        ItemAdapter.Listener callback = callbackWeakRef.get();
        if (callback != null) callback.onClickDeleteButton(getAdapterPosition());
    }

    @OnClick(R.id.modify_currency)
    public void onClickModifyCurrencyButton() {
        String priceInDollars = item.getPriceInDollar();
        String[] arrayDollars = priceInDollars.split("\\$");
        String priceInDollarsSplitted = arrayDollars[0].trim();
        int priceInDollarsToConvert = Integer.parseInt(priceInDollarsSplitted);
        int priceInDollarConvertedInEuros = Utils.convertDollarToEuro(priceInDollarsToConvert);

        if (isDollarCurrencyDisplayed) {
            String priceInEurosToString = String.valueOf(priceInDollarConvertedInEuros);
            this.priceProperty.setText(itemView.getResources().getString(R.string.dollar_sign_string, priceInEurosToString));
            isDollarCurrencyDisplayed = false;
        } else {
            this.priceProperty.setText(priceInDollars);
            isDollarCurrencyDisplayed = true;
        }
    }

    @OnClick(R.id.modify_property)
    public void onClickModifyPropertyButton() {
        Intent intent = new Intent(context, EditPropertyActivity.class);
        //passer l'item avec le parcelable
        intent.putExtra("Item", Parcels.wrap(item));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}