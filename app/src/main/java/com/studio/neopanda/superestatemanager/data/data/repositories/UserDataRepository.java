package com.studio.neopanda.superestatemanager.data.data.repositories;

import androidx.lifecycle.LiveData;

import com.studio.neopanda.superestatemanager.data.data.dao.UserDao;
import com.studio.neopanda.superestatemanager.data.data.models.User;

public class UserDataRepository {

    private final UserDao userDao;

    public UserDataRepository(UserDao userDao) {
        this.userDao = userDao;
    }

    // --- GET USER ---
    public LiveData<User> getUser(long userId) {
        return this.userDao.getUser(userId);
    }

    public void createUser(User user) {
        userDao.createUser(user);
    }
}