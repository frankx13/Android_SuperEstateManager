package com.studio.neopanda.superestatemanager.searchproperty;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.data.data.models.User;
import com.studio.neopanda.superestatemanager.data.data.repositories.ItemDataRepository;
import com.studio.neopanda.superestatemanager.data.data.repositories.UserDataRepository;

import java.util.List;

public class SearchViewModel extends ViewModel {

    // REPOSITORIES
    private final ItemDataRepository itemDataSource;
    private final UserDataRepository userDataSource;

    // DATA
    @Nullable
    private LiveData<User> currentUser;

    public SearchViewModel(ItemDataRepository itemDataSource, UserDataRepository userDataSource) {
        this.itemDataSource = itemDataSource;
        this.userDataSource = userDataSource;
    }

    public void init(long userId) {
        if (this.currentUser != null) {
            return;
        }
        currentUser = userDataSource.getUser(userId);
    }

    // -------------
    // FOR ITEM
    // -------------

    public LiveData<List<Item>> searchItems(long userId,
                                            int priceInDollarMax,
                                            int priceInDollarMin,
                                            int surfaceInSquareMetersMax,
                                            int surfaceInSquareMetersMin,
                                            int numberOfRoomsMax,
                                            int numberOfRoomsMin) {
        return itemDataSource.searchItems(userId,
                priceInDollarMax,
                priceInDollarMin,
                surfaceInSquareMetersMax,
                surfaceInSquareMetersMin,
                numberOfRoomsMax,
                numberOfRoomsMin);
    }
}
