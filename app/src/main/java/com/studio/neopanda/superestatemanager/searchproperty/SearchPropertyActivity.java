package com.studio.neopanda.superestatemanager.searchproperty;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.data.data.injection.Injection;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactorySearch;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SearchPropertyActivity extends BaseActivity implements SearchAdapter.Listener {

    private static int USER_ID = 1;
    //UI
    @BindView(R.id.edit_text_min_price_search)
    EditText minPriceSearchET;
    @BindView(R.id.et_max_price_search)
    EditText maxPriceSearchET;
    @BindView(R.id.et_min_sqm_search)
    EditText minSqmSearchET;
    @BindView(R.id.et_max_sqm_search)
    EditText maxSqmSearchET;
    @BindView(R.id.et_min_rooms_search)
    EditText minRoomsSearchET;
    @BindView(R.id.et_max_rooms_search)
    EditText maxRoomsSearchET;
    @BindView(R.id.properties_recyclerview_search)
    RecyclerView recyclerViewSearch;
    //DATA
    private SearchViewModel searchViewModel;
    private SearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_search_property;
    }


    //Configuring ViewModel
    private void configureViewModel() {
        ViewModelFactorySearch mViewModelFactorySearch = Injection.provideViewModelFactorySearch(this);
        this.searchViewModel = ViewModelProviders.of(this, mViewModelFactorySearch).get(SearchViewModel.class);
        this.searchViewModel.init(USER_ID);
    }

    //Get all items for a specific search
    private void searchItems(int userId,
                             int priceMax,
                             int priceMin,
                             int surfaceSqmMax,
                             int surfaceSqmMin,
                             int numberRoomsMax,
                             int numberRoomsMin) {
        this.searchViewModel.searchItems(userId,
                priceMax,
                priceMin,
                surfaceSqmMax,
                surfaceSqmMin,
                numberRoomsMax,
                numberRoomsMin)
                .observe(this, this::updateItemsList);
    }

    //Update the list of items
    private void updateItemsList(List<Item> items) {
        this.adapter.updateData(items);
    }

    @Override
    public void onClickDeleteButton(int position) {
    }

    private void configureRecyclerViewSearch() {
        this.adapter = new SearchAdapter(this);
        this.recyclerViewSearch.setAdapter(this.adapter);
        this.recyclerViewSearch.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.btn_launch_search)
    public void onClickSearchBtn() {

        int searchMinPrice = 0;
        int searchMaxPrice = 0;
        int searchMinSqm = 0;
        int searchMaxSqm = 0;
        int searchMinRooms = 0;
        int searchMaxRooms = 0;

        int counterPriceFactors = 0;
        int counterSqmFactors = 0;
        int counterRoomsFactors = 0;
        int counterTotalFactors;

        try {
            if (minPriceSearchET.getEditableText().toString() != null && !minPriceSearchET.getEditableText().toString().isEmpty()) {
                searchMinPrice = Integer.parseInt(minPriceSearchET.getEditableText().toString());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        try {
            if (maxPriceSearchET.getEditableText().toString() != null && !maxPriceSearchET.getEditableText().toString().isEmpty()) {
                searchMaxPrice = Integer.parseInt(maxPriceSearchET.getEditableText().toString());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            if (minSqmSearchET.getEditableText().toString() != null && !minSqmSearchET.getEditableText().toString().isEmpty()) {
                searchMinSqm = Integer.parseInt(minSqmSearchET.getEditableText().toString());
            }

        } catch (NumberFormatException ignored) {
        }

        try {
            if (maxSqmSearchET.getEditableText().toString() != null && !maxSqmSearchET.getEditableText().toString().isEmpty()) {
                searchMaxSqm = Integer.parseInt(maxSqmSearchET.getEditableText().toString());
            }

        } catch (NumberFormatException ignored) {
        }

        try {
            if (maxRoomsSearchET.getEditableText().toString() != null && !maxRoomsSearchET.getEditableText().toString().isEmpty()) {
                searchMaxRooms = Integer.parseInt(maxRoomsSearchET.getEditableText().toString());
            }

        } catch (NumberFormatException ignored) {
        }

        try {
            if (minRoomsSearchET.getEditableText().toString() != null && !minRoomsSearchET.getEditableText().toString().isEmpty()) {
                searchMinRooms = Integer.parseInt(minRoomsSearchET.getEditableText().toString());
            }

        } catch (NumberFormatException ignored) {
        }

        // Make loops to determine how and which search factors are filled by the user
        int[] searchPrice = {searchMinPrice, searchMaxPrice};
        int[] searchSqm = {searchMinSqm, searchMaxSqm};
        int[] searchRooms = {searchMinRooms, searchMaxRooms};
        int i;

        for (i = 0; i < searchPrice.length; i++) {
            if (searchPrice[i] != 0) {
                counterPriceFactors++;
            }
        }
        for (i = 0; i < searchSqm.length; i++) {
            if (searchSqm[i] != 0) {
                counterSqmFactors++;
            }
        }
        for (i = 0; i < searchRooms.length; i++) {
            if (searchRooms[i] != 0) {
                counterRoomsFactors++;
            }
        }

        //Adding all factors to one
        counterTotalFactors = counterPriceFactors + counterSqmFactors + counterRoomsFactors + counterPriceFactors;

        //Checking that at least 2 factors are assigned
        if (counterTotalFactors >= 2) {
            if (counterPriceFactors == 2 || counterSqmFactors == 2 || counterRoomsFactors == 2) {
                //Configure ViewModel and RV
                this.configureViewModel();

                Toast.makeText(this, "Searching..", Toast.LENGTH_LONG).show();
                this.searchItems(USER_ID, searchMaxPrice, searchMinPrice, searchMaxSqm, searchMinSqm, searchMaxRooms, searchMinRooms);
                this.configureRecyclerViewSearch();
            } else {
                Toast.makeText(this, "You must fill at least 2 search factors from the same type to launch a search !", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "You must fill at least 2 search factors in order to launch a search!", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.back_btn_search_property)
    public void onClickBackBtn() {
        Toast.makeText(this, "Going back to the list!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.info_about_search_panel)
    public void onClickInfoSearchButton() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("The search motor works in pair : for example, if you want to make a search based on the price, you need to fill both the minimum and maximum price to query!")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) ->
                        Toast.makeText(this, "Cool!", Toast.LENGTH_SHORT).show());
        AlertDialog alert = builder.create();
        alert.show();
    }
}
