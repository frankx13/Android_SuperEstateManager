package com.studio.neopanda.superestatemanager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.data.data.injection.Injection;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactory;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;
import com.studio.neopanda.superestatemanager.listproperty.ItemAdapter;
import com.studio.neopanda.superestatemanager.listproperty.ItemViewModel;
import com.studio.neopanda.superestatemanager.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AddPropertyActivity extends BaseActivity implements ItemAdapter.Listener {

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static int USER_ID = 1;
    private static int GALLERY_REQUEST_CODE = 1;

    //UI
    @BindView(R.id.add_property_address)
    EditText propertyAddressET;
    @BindView(R.id.add_property_type)
    EditText propertyTypeET;
    @BindView(R.id.add_property_price)
    EditText propertyPriceET;
    @BindView(R.id.add_property_surface)
    EditText propertySurfaceET;
    @BindView(R.id.add_property_description)
    EditText propertyDescET;
    @BindView(R.id.add_property_name_agent)
    EditText propertyAffectedAgentET;
    @BindView(R.id.add_property_number_rooms)
    EditText propertyNumberOfRoomsET;

    //DATA
    private ItemViewModel itemViewModel;
    private List<String> urisOfImageGalleryForRooms;
    private int URI_REQUEST_CODE;
    private String uriOfImageGallery = "";
    private Uri uriForCoverString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        urisOfImageGalleryForRooms = new ArrayList<>();
        this.configureViewModel();
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_add_property;
    }


    // -------------------
    // DATA
    // -------------------

    //Configuring ViewModel
    private void configureViewModel() {
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(this);
        this.itemViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ItemViewModel.class);
        this.itemViewModel.init(USER_ID);
    }

    //Create a new item
    private void createItem() {
        int fieldsCounter = 0;
        String propertyPrice = "";
        String propertyType = "";
        String propertyAddress = "";
        String propertyImage = "";
        int numberOfRooms = 0;
        int surfaceInSquareMeters = 0;
        String fullDescOfProperty = "";
        String nameAgentOfProperty = "";
        String dateArrivalOnMarket = "";
        Boolean propertyStatus = false;
        List<String> imagesForRooms = null;

        if ((propertyPriceET.getEditableText().toString() + " $").trim().equals("$")) {
            Toast.makeText(this, "You must enter a price to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            propertyPrice = propertyPriceET.getEditableText().toString() + " $";
            fieldsCounter += 1;
        }

        if (propertyTypeET.getEditableText().toString().equals("")) {
            Toast.makeText(this, "You must enter a type to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            propertyType = propertyTypeET.getEditableText().toString();
            fieldsCounter += 1;
        }

        if (propertyAddressET.getEditableText().toString().equals("Country, Town")
                || propertyAddressET.getEditableText().toString().equals("")) {
            Toast.makeText(this, "You must enter an address to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            propertyAddress = propertyAddressET.getEditableText().toString();
            fieldsCounter += 1;
        }

        if (uriOfImageGallery.equals("")) {
            Toast.makeText(this, "You must add a cover picture to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            propertyImage = uriOfImageGallery;
            fieldsCounter += 1;
        }

        if ((propertyNumberOfRoomsET.getEditableText().toString().equals(""))) {
            Toast.makeText(this, "You must enter the number of rooms to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            numberOfRooms = Integer.parseInt(propertyNumberOfRoomsET.getEditableText().toString());
            fieldsCounter += 1;
        }

        if (propertySurfaceET.getEditableText().toString().equals("")) {
            Toast.makeText(this, "You must enter the surface in square meters to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            surfaceInSquareMeters = Integer.parseInt(propertySurfaceET.getEditableText().toString());
            fieldsCounter += 1;
        }

        if (propertyDescET.getEditableText().toString().equals("")) {
            Toast.makeText(this, "You must enter a description to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            fullDescOfProperty = propertyDescET.getEditableText().toString();
            fieldsCounter += 1;
        }

        if (propertyAffectedAgentET.getEditableText().toString().equals("")) {
            Toast.makeText(this, "You must enter an agent name to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            nameAgentOfProperty = propertyAffectedAgentET.getEditableText().toString();
            fieldsCounter += 1;
        }

        if (!Utils.getTodayDate().equals("")) {
            dateArrivalOnMarket = Utils.getTodayDate();
        }

        if (urisOfImageGalleryForRooms.size() == 0) {
            Toast.makeText(this, "You must add at least one room image to create a new property!", Toast.LENGTH_SHORT).show();
        } else {
            imagesForRooms = urisOfImageGalleryForRooms;
            fieldsCounter += 1;
        }

        if (fieldsCounter >= 9) {
            Item item = new Item(USER_ID,
                    numberOfRooms,
                    surfaceInSquareMeters,
                    propertyPrice,
                    propertyType,
                    propertyAddress,
                    propertyImage,
                    fullDescOfProperty,
                    nameAgentOfProperty,
                    dateArrivalOnMarket,
                    propertyStatus,
                    imagesForRooms);

            this.itemViewModel.createItem(item);
            Toast.makeText(this, "Property successfully added!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    // ----ACTIONS----

    @OnClick(R.id.add_property_back_btn)
    public void onClickBackToList() {
        Toast.makeText(this, "Going to the list!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.add_property_cover_gallery_btn)
    public void addImageCoverWithGallery() {
        //Used to import images for the property main image
        pickFromGallery(1);
    }

    @OnClick(R.id.add_property_rooms_gallery_btn)
    public void addImageRoomWithGallery() {
        //Used to import images for the property detailed images
        pickFromGallery(2);
    }

    @OnClick(R.id.add_property_cover_picture_btn)
    public void addImageCoverWithPicture() {
        dispatchTakePictureIntent(3);
    }

    @OnClick(R.id.add_property_rooms_picture_btn)
    public void addImageRoomWithPicture() {
        dispatchTakePictureIntent(4);
    }

    @OnClick(R.id.add_property_create_btn)
    public void onClickAddButton() {
        //Create item after user clicked on button
        this.createItem();
    }

    @OnClick(R.id.info_about_address_add_property)
    public void onClickInfoAddressButton() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Here, it's important to separate the country and the town by a coma!")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> Toast.makeText(this, "Nice!", Toast.LENGTH_SHORT).show());
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClickDeleteButton(int position) {
    }


    // ----GALLERY----

    @SuppressLint("ObsoleteSdkInt")
    private void pickFromGallery(int galleryCode) {
        URI_REQUEST_CODE = galleryCode;
        Intent intent;
        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        }
    }

    // ----CAMERA----
    private void dispatchTakePictureIntent(int captureCode) {
        URI_REQUEST_CODE = captureCode;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.studio.neopanda.superestatemanager",
                        photoFile);
                uriForCoverString = photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // Save a file: path for use with ACTION_VIEW intents
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {
                if (URI_REQUEST_CODE == 1) {
                    Uri selectedImage = data.getData();
                    if (selectedImage != null) {
                        uriOfImageGallery = selectedImage.toString();
                        Toast.makeText(this, selectedImage.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 2) {
                    Uri selectedImage = data.getData();
                    if (selectedImage != null) {
                        urisOfImageGalleryForRooms.add(selectedImage.toString());
                        Toast.makeText(this, selectedImage.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 3) {
                    if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                        uriOfImageGallery = uriForCoverString.toString();
                        Toast.makeText(this, uriForCoverString.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (URI_REQUEST_CODE == 4) {
                    if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                        urisOfImageGalleryForRooms.add(uriForCoverString.toString());
                        Toast.makeText(this, uriForCoverString.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "GET_ACCOUNTS Denied",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions,
                    grantResults);
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                (dialog, which) -> ActivityCompat.requestPermissions((Activity) context,
                        new String[]{permission},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE));
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}
