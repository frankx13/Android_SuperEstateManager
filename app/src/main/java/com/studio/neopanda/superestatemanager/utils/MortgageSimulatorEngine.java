package com.studio.neopanda.superestatemanager.utils;

public class MortgageSimulatorEngine {

    public static int calculateMonthlyPayment(
            int loanAmount,
            int termInYears,
            double interestRate) {

        // Convert interest rate into a decimal
        // eg. 6.5% = 0.065

        interestRate /= 100.0;

        // Monthly interest rate
        // is the yearly rate divided by 12

        double monthlyRate = interestRate / 12.0;

        // The length of the term in months
        // is the number of years times 12

        int termInMonths = termInYears * 12;

        // Calculate the monthly payment
        // Typically this formula is provided so
        // we won't go into the details

        // The Math.pow() method is used calculate values raised to a power
        double result = (loanAmount * monthlyRate) /
                (1 - Math.pow(1 + monthlyRate, -termInMonths));
        int resultRounded = (int) result;

        return resultRounded;
    }

    public static int calculateTotalSumOfLoan(
            int loanAmount,
            int termInYears,
            double interestRate) {

        interestRate /= 100.0;
        double monthlyRate = interestRate / 12.0;
        int termInMonths = termInYears * 12;
        double monthlyPayment =
                (loanAmount * monthlyRate) /
                        (1 - Math.pow(1 + monthlyRate, -termInMonths));

        double result = monthlyPayment * termInMonths;
        int resultRounded = (int) result;

//        return monthlyPayment * termInMonths;
        return resultRounded;
    }
}

