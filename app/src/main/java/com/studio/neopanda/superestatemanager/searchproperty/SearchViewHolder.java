package com.studio.neopanda.superestatemanager.searchproperty;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.DetailPropertyFragment;
import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.img_property_search)
    ImageView imageProperty;
    @BindView(R.id.type_property_search)
    TextView typeProperty;
    @BindView(R.id.localisation_property_search)
    TextView localisationProperty;
    @BindView(R.id.price_property_search)
    TextView priceProperty;
    @BindView(R.id.sqm_property_search)
    TextView sqmProperty;
    @BindView(R.id.rooms_property_search)
    TextView roomsProperty;
    @BindView(R.id.date_sold_property_search)
    TextView dateSoldPropertyTV;
    @BindView(R.id.img_property_when_sold_search)
    TextView labelSoldTV;


    // FOR DATA
    private WeakReference<SearchAdapter.Listener> callbackWeakRef;
    private Context context = itemView.getContext();

    public SearchViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(v -> {
            //TODO - Implement the ongoing to DetailActivity when an item is clicked
        });
    }

    public void updateWithItem(Item item, SearchAdapter.Listener callback) {
        this.callbackWeakRef = new WeakReference<>(callback);

        if (!item.getImageOfProperty().equals("")) {
            this.imageProperty.setImageURI(Uri.parse(item.getImageOfProperty()));
        } else {
            this.imageProperty.setBackgroundResource(R.drawable.ic_edit_black_24dp);
        }

        if (item.isSold()) {
            dateSoldPropertyTV.setText(itemView.getResources().getString(R.string.sold_the_date_sold, item.getDateSold()));
            dateSoldPropertyTV.setVisibility(View.VISIBLE);
            labelSoldTV.setVisibility(View.VISIBLE);
        }

        this.typeProperty.setText(item.getTypeOfProperty());
        String[] splittedAddressComplete = item.getAddressOfProperty().split(",");
        String splittedAddresspart = splittedAddressComplete[0];
        this.localisationProperty.setText(splittedAddresspart);
        this.priceProperty.setText(item.getPriceInDollar());
        this.sqmProperty.setText(itemView.getResources()
                .getString(R.string.search_factor_sqm, item.getSurfaceInSquareMeters()));
        this.roomsProperty.setText(itemView.getResources()
                .getString(R.string.search_factor_rooms, item.getNumberOfRooms()));
    }

    public void handleClickOnItem(Item item) {
        itemView.setOnClickListener(v -> {
            Toast.makeText(context, "Going to the property detail!", Toast.LENGTH_SHORT).show();
            AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
            Fragment f = DetailPropertyFragment.newInstance(item);
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.detailContainerFromSearch, f).addToBackStack(null).commit();
        });
    }

    @Override
    public void onClick(View view) {
        SearchAdapter.Listener callback = callbackWeakRef.get();
        if (callback != null) callback.onClickDeleteButton(getAdapterPosition());
    }
}
