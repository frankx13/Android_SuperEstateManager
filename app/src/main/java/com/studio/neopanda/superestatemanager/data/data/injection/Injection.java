package com.studio.neopanda.superestatemanager.data.data.injection;

import android.content.Context;

import com.studio.neopanda.superestatemanager.data.data.RealEstateManagerDatabase;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactory;
import com.studio.neopanda.superestatemanager.data.data.injections.ViewModelFactorySearch;
import com.studio.neopanda.superestatemanager.data.data.repositories.ItemDataRepository;
import com.studio.neopanda.superestatemanager.data.data.repositories.UserDataRepository;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Injection {

    public static ItemDataRepository provideItemDataSource(Context context) {
        RealEstateManagerDatabase database = RealEstateManagerDatabase.getInstance(context);
        return new ItemDataRepository(database.itemDao());
    }

    public static UserDataRepository provideUserDataSource(Context context) {
        RealEstateManagerDatabase database = RealEstateManagerDatabase.getInstance(context);
        return new UserDataRepository(database.userDao());
    }

    public static Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        ItemDataRepository dataSourceItem = provideItemDataSource(context);
        UserDataRepository dataSourceUser = provideUserDataSource(context);
        Executor executor = provideExecutor();
        return new ViewModelFactory(dataSourceItem, dataSourceUser, executor);
    }

    public static ViewModelFactorySearch provideViewModelFactorySearch(Context context) {
        ItemDataRepository dataSourceItem = provideItemDataSource(context);
        UserDataRepository dataSourceUser = provideUserDataSource(context);
        Executor executor = provideExecutor();
        return new ViewModelFactorySearch(dataSourceItem, dataSourceUser);
    }
}