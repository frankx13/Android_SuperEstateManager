package com.studio.neopanda.superestatemanager.data.data;

import android.content.ContentValues;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.studio.neopanda.superestatemanager.data.data.dao.ItemDao;
import com.studio.neopanda.superestatemanager.data.data.dao.UserDao;
import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.data.data.models.User;

@Database(entities = {Item.class, User.class}, version = 7, exportSchema = false)
public abstract class RealEstateManagerDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile RealEstateManagerDatabase INSTANCE;

    // --- INSTANCE ---
    public static RealEstateManagerDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (RealEstateManagerDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RealEstateManagerDatabase.class, "MyDatabase.db")
//                            .addCallback(prepopulateDatabase())
                            //To delete all data in the database when migration is needed
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback prepopulateDatabase() {
        return new Callback() {

            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);

                ContentValues contentValues = new ContentValues();
                contentValues.put("id", 1);
                contentValues.put("username", "François");
                contentValues.put("urlPicture", "https://static.lexpress.fr/medias_11568/w_2048,h_1146,c_crop,x_0,y_160/w_960,h_540,c_fill,g_north/v1509975901/panda-chine_5923268.jpg?auto=compress,format&q=80&h=100&dpr=2");

                db.insert("User", OnConflictStrategy.IGNORE, contentValues);
            }
        };
    }

    // --- DAO ---
    public abstract ItemDao itemDao();

    public abstract UserDao userDao();
}