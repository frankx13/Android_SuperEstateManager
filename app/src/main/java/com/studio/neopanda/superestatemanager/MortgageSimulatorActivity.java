package com.studio.neopanda.superestatemanager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.studio.neopanda.superestatemanager.base.BaseActivity;
import com.studio.neopanda.superestatemanager.listproperty.DashboardActivity;
import com.studio.neopanda.superestatemanager.utils.MortgageSimulatorEngine;
import com.studio.neopanda.superestatemanager.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class MortgageSimulatorActivity extends BaseActivity {

    //UI
    @BindView(R.id.et_simulator_value_loan)
    EditText loanValueET;
    @BindView(R.id.et_simulator_duration_loan)
    EditText loanDurationET;
    @BindView(R.id.et_simulator_interest_loan)
    EditText loanInterestET;
    @BindView(R.id.tv_monthly_loan_amount)
    TextView monthlyLoanAmountTV;
    @BindView(R.id.tv_total_loan_amount)
    TextView totalLoanAmountTV;
    @BindView(R.id.navigation_view_simulator)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout_simulator)
    DrawerLayout drawerLayout;
    private boolean isInternetAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbar();
        setNavigationDrawer();
    }

    @Override
    public int getLayoutContentViewID() {
        return R.layout.activity_mortgage_simulator;
    }

    @OnClick(R.id.loan_simulation_btn)
    public void onClickLoanSimulationBtn() {
        if (!loanValueET.getEditableText().toString().isEmpty()
                & !loanDurationET.getEditableText().toString().isEmpty()
                & !loanInterestET.getEditableText().toString().isEmpty()) {
            int loanValue = Integer.parseInt(loanValueET.getEditableText().toString());
            int loanDuration = Integer.parseInt(loanDurationET.getEditableText().toString());
            double loanInterest = Double.parseDouble(loanInterestET.getEditableText().toString());

            int monthlyLoan = MortgageSimulatorEngine.calculateMonthlyPayment(
                    loanValue,
                    loanDuration,
                    loanInterest);

            int totalLoan = MortgageSimulatorEngine.calculateTotalSumOfLoan(loanValue, loanDuration, loanInterest);
            Log.i("", "onClickLoanSimulationBtn: " + totalLoan);

            monthlyLoanAmountTV.setText("The amount of your monthly paiements would be : " + monthlyLoan + " $");
            totalLoanAmountTV.setText("The total cost of your credit would be : " + totalLoan + " $");
        } else {
            Toast.makeText(this, "Please indicate a correct value, duration and interest in order to launch the simulation !", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.mortgage_simulator_back_btn)
    public void onClickBackBtn() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //TOOLBAR
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_top_simulator);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    //Toolbar functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return true;
    }

    //NAVIGATION DRAWER
    private void setNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            // set item as selected to persist highlight
            menuItem.setChecked(true);

            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.properties_dashboard_nd_simulator:
                    Toast.makeText(this, "Going to the dashboard!", Toast.LENGTH_SHORT).show();
                    Intent intentSimulator = new Intent(this, DashboardActivity.class);
                    startActivity(intentSimulator);
                    finish();
                    break;
                case R.id.properties_map_nd_simulator:
                    if (Utils.isConnectedToNetwork(this, isInternetAvailable)) {
                        Toast.makeText(this, "Going to the interactive map!", Toast.LENGTH_SHORT).show();
                        Intent intentMap = new Intent(this, AgentMapsActivity.class);
                        startActivity(intentMap);
                        finish();
                    } else {
                        Toast.makeText(this, "Oops ! It seems you do not receive Internet. Try again when you get some!", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case R.id.app_menu_nd_simulator:
                    Toast.makeText(this, "Going to the main menu!", Toast.LENGTH_SHORT).show();
                    Intent intentMenu = new Intent(this, MainActivity.class);
                    startActivity(intentMenu);
                    finish();
                    break;

                case R.id.test_connectivity_menu_nd_simulator:
                    Toast.makeText(this, "Going to the Internet tester!", Toast.LENGTH_SHORT).show();
                    Intent intentInternetTest = new Intent(this, TestMyWifi.class);
                    startActivity(intentInternetTest);
                    finish();
                    break;
            }
            return true;
        });
    }
}
