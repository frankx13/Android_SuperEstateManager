package com.studio.neopanda.superestatemanager.data.data.models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.studio.neopanda.superestatemanager.utils.MyCustomTypeConverter;

import org.parceler.Parcel;

import java.util.List;

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "userId"))
@Parcel
public class Item {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private long userId;
    private int surfaceInSquareMeters;
    private int numberOfRooms;
    private String imageOfProperty;
    private String priceInDollar;
    private String typeOfProperty;
    private String fullDescOfProperty;
    private String addressOfProperty;
    private String dateArrivalOnMarket;
    private String dateSold;
    private String nameAgentOfProperty;
    private boolean isSold;
    @TypeConverters(MyCustomTypeConverter.class)
    private List<String> imagesOfRooms;

    public Item() {
    }

    public Item(long userId,
                int numberOfRooms,
                int surfaceInSquareMeters,
                String priceInDollar,
                String typeOfProperty,
                String addressOfProperty,
                String imageOfProperty,
                String fullDescOfProperty,
                String nameAgentOfProperty,
                String dateArrivalOnMarket,
                Boolean propertyStatus,
                List<String> imagesOfRooms) {

        this.userId = userId;
        this.numberOfRooms = numberOfRooms;
        this.surfaceInSquareMeters = surfaceInSquareMeters;
        this.priceInDollar = priceInDollar;
        this.typeOfProperty = typeOfProperty;
        this.addressOfProperty = addressOfProperty;
        this.imageOfProperty = imageOfProperty;
        this.fullDescOfProperty = fullDescOfProperty;
        this.nameAgentOfProperty = nameAgentOfProperty;
        this.dateArrivalOnMarket = dateArrivalOnMarket;
        this.isSold = propertyStatus;
        this.imagesOfRooms = imagesOfRooms;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getImageOfProperty() {
        return imageOfProperty;
    }

    public void setImageOfProperty(String imageOfProperty) {
        this.imageOfProperty = imageOfProperty;
    }

    public String getPriceInDollar() {
        return priceInDollar;
    }

    public void setPriceInDollar(String priceInDollar) {
        this.priceInDollar = priceInDollar;
    }

    public int getSurfaceInSquareMeters() {
        return surfaceInSquareMeters;
    }

    public void setSurfaceInSquareMeters(int surfaceInSquareMeters) {
        this.surfaceInSquareMeters = surfaceInSquareMeters;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getTypeOfProperty() {
        return typeOfProperty;
    }

    public void setTypeOfProperty(String typeOfProperty) {
        this.typeOfProperty = typeOfProperty;
    }

    public String getFullDescOfProperty() {
        return fullDescOfProperty;
    }

    public void setFullDescOfProperty(String fullDescOfProperty) {
        this.fullDescOfProperty = fullDescOfProperty;
    }

    public String getAddressOfProperty() {
        return addressOfProperty;
    }

    public void setAddressOfProperty(String addressOfProperty) {
        this.addressOfProperty = addressOfProperty;
    }

    public String getDateArrivalOnMarket() {
        return dateArrivalOnMarket;
    }

    public void setDateArrivalOnMarket(String dateArrivalOnMarket) {
        this.dateArrivalOnMarket = dateArrivalOnMarket;
    }

    public String getDateSold() {
        return dateSold;
    }

    public void setDateSold(String dateSold) {
        this.dateSold = dateSold;
    }

    public String getNameAgentOfProperty() {
        return nameAgentOfProperty;
    }

    public void setNameAgentOfProperty(String nameAgentOfProperty) {
        this.nameAgentOfProperty = nameAgentOfProperty;
    }

    public List<String> getImagesOfRooms() {
        return imagesOfRooms;
    }

    public void setImagesOfRooms(List<String> imagesOfRooms) {
        this.imagesOfRooms = imagesOfRooms;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }
}