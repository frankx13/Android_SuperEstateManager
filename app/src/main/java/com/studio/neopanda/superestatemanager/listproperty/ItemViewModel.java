package com.studio.neopanda.superestatemanager.listproperty;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.studio.neopanda.superestatemanager.data.data.models.Item;
import com.studio.neopanda.superestatemanager.data.data.models.User;
import com.studio.neopanda.superestatemanager.data.data.repositories.ItemDataRepository;
import com.studio.neopanda.superestatemanager.data.data.repositories.UserDataRepository;

import java.util.List;
import java.util.concurrent.Executor;

public class ItemViewModel extends ViewModel {

    // REPOSITORIES
    private final ItemDataRepository itemDataSource;
    private final UserDataRepository userDataSource;
    private final Executor executor;

    // DATA
    @Nullable
    private LiveData<User> currentUser;

    public ItemViewModel(ItemDataRepository itemDataSource, UserDataRepository userDataSource, Executor executor) {
        this.itemDataSource = itemDataSource;
        this.userDataSource = userDataSource;
        this.executor = executor;
    }

    public void init(long userId) {
        if (this.currentUser != null) {
            return;
        }
        currentUser = userDataSource.getUser(userId);
    }

    // -------------
    // FOR USER
    // -------------

    public LiveData<User> getUser(long userId) {
        return this.currentUser;
    }

    public void createUser(User user){
        executor.execute(() -> userDataSource.createUser(user));
    }

    // -------------
    // FOR ITEM
    // -------------

    public LiveData<List<Item>> getItems(long userId) {
        return itemDataSource.getItems(userId);
    }

    public void createItem(Item item) {
        executor.execute(() -> itemDataSource.createItem(item));
    }

    public void updateItem(Item item) {
        executor.execute(() -> itemDataSource.updateItem(item));
    }
}