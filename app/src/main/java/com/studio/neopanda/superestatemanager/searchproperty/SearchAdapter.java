package com.studio.neopanda.superestatemanager.searchproperty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.R;
import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder> {

    private final SearchAdapter.Listener callback;
    //DATA
    private List<Item> items;

    //CONSTRUCTOR
    public SearchAdapter(SearchAdapter.Listener callback) {
        this.items = new ArrayList<>();
        this.callback = callback;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_search_property_item, parent, false);

        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder viewHolder, int position) {
        viewHolder.updateWithItem(this.items.get(position), this.callback);
        viewHolder.handleClickOnItem(this.items.get(position));
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    //Update items
    public void updateData(List<Item> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    // CALLBACK
    public interface Listener {
        void onClickDeleteButton(int position);
    }
}