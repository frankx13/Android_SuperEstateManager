package com.studio.neopanda.superestatemanager;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studio.neopanda.superestatemanager.data.data.models.Item;

import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {

    private Context mContext;
    private List<String> mData;
    private List<Item> itemList;

    public DetailAdapter(Context mContext, List<String> mData, List<Item> itemList) {
        this.mContext = mContext;
        this.mData = mData;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.fragment_detail_property_item, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (mData.size() < 2) {
            holder.btnDeleteImage.setVisibility(View.INVISIBLE);
        }
        holder.imageOfRoom.setImageURI(Uri.parse(mData.get(position)));
        holder.imageOfRoom.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        holder.btnDeleteImage.setOnClickListener(v -> {
            mData.remove(mData.get(position));
//                    itemList.get(position).setImagesOfRooms(mData);
            notifyItemRemoved(mData.indexOf(mData.get(position)));
            notifyItemRangeRemoved(mData.indexOf(mData.get(position)), mData.size());
            Toast.makeText(mContext, "Image successfully removed!", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return ((mData == null) ? 0 : mData.size());
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageOfRoom;
        Button btnDeleteImage;
        RelativeLayout itemContainer;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageOfRoom = itemView.findViewById(R.id.IV_image_detail_property);
            btnDeleteImage = itemView.findViewById(R.id.btn_delete_image);
            itemContainer = itemView.findViewById(R.id.detail_item_container);
        }
    }
}
