package com.studio.neopanda.superestatemanager;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.studio.neopanda.superestatemanager.utils.Utils;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class UtilsInstrumentedTest {

    @Test
    public void usingNetworkCheckMethod() {
        Context instrumentationCtx = InstrumentationRegistry.getInstrumentation().getContext();
        assertTrue(Utils.isConnectedToNetwork(instrumentationCtx));
    }
}
