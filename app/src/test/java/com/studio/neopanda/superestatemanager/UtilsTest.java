package com.studio.neopanda.superestatemanager;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class UtilsTest {


    @Test
    public void convertDollarToEuro() {
        int dollars = 150000;
        long result = Math.round(dollars * 0.90);
        assertEquals(135000, result);
    }

    @Test
    public void convertEuroToDollars() {
        int euro = 150000;
        long result = Math.round(euro * 1.11);
        assertEquals(166500, result);
    }

    @Test
    public void getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = dateFormat.format(new Date());
        assertEquals("20/08/2019", date);
    }
}